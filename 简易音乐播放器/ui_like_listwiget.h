/********************************************************************************
** Form generated from reading UI file 'like_listwiget.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIKE_LISTWIGET_H
#define UI_LIKE_LISTWIGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_like_listwiget
{
public:
    QListWidget *like_list;

    void setupUi(QWidget *like_listwiget)
    {
        if (like_listwiget->objectName().isEmpty())
            like_listwiget->setObjectName("like_listwiget");
        like_listwiget->resize(391, 453);
        like_list = new QListWidget(like_listwiget);
        like_list->setObjectName("like_list");
        like_list->setGeometry(QRect(50, 30, 291, 341));

        retranslateUi(like_listwiget);

        QMetaObject::connectSlotsByName(like_listwiget);
    } // setupUi

    void retranslateUi(QWidget *like_listwiget)
    {
        like_listwiget->setWindowTitle(QCoreApplication::translate("like_listwiget", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class like_listwiget: public Ui_like_listwiget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIKE_LISTWIGET_H
