#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QUrl>
#include<QListWidgetItem>
class QAudioOutput;
class QMediaPlayer;
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    int play_cur;//歌单数量索引
       char*str_next[128];//指针数组储存歌曲名下一首用
       char*str_up[128];//指针数组储存歌曲名上一首用

private slots:
    void on_pushButton_clicked();//文件1

    void on_pushButton_2_clicked();//开始，暂停1

    void on_pushButton_3_clicked();//播放模式3

    void on_pushButton_4_clicked();//音量图标2

    void on_pushButton_5_clicked();//下一首3

    void on_pushButton_6_clicked();//上一首3

    void on_verticalSlider_sliderMoved(int position);//音乐进度条1

    void on_pushButton_7_clicked();//删除列表中的歌曲2

    void on_verticalSlider_sliderReleased();//音量大小控制图标释放2

    void on_pushButton_8_clicked();//本地菜单2

    void on_pushButton_9_clicked();//收藏菜单2

    void on_listWidget_local_itemDoubleClicked(QListWidgetItem *item);//双击收藏音乐2

    void on_listWidget_local_currentRowChanged(int currentRow);//本地音乐点击播放 1

    void on_listWidget_history_currentRowChanged(int currentRow);//收藏音乐点击播放1

private:
    Ui::Widget *ui;
    QList<QUrl>playlist;//播放列表
    QAudioOutput* audioOutput;//音频文件输出
    QMediaPlayer* mediaPlayer;//音频播放
    int curplayindex;//当前列表行下标
    QString durationTime;//音乐总时长
    QString positionTime;//音乐当前播放时长
};
#endif // WIDGET_H
