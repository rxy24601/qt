#include "widget.h"
#include "ui_widget.h"
#include<QDebug>
#include<QFileDialog>
#include<QDir>
#include<QMediaPlayer>//音频播放
#include<QAudioOutput>//音乐数据输出
#include<QUrl>//音乐地址连接
#include <QRandomGenerator>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //播放音乐
    audioOutput=new QAudioOutput(this);
    mediaPlayer=new QMediaPlayer(this);
    mediaPlayer->setAudioOutput(audioOutput);
    //总时长
    connect(mediaPlayer,&QMediaPlayer::durationChanged,this,[=](qint64 duration)
    {
        ui->totallabel->setText(QString("%1:%2").arg(duration/1000/60,2,10,QChar('0')).arg(duration/1000%60));
        ui->horizontalSlider->setRange(0,duration);//设置进度条范围
    });
    //当前时长
    connect(mediaPlayer,&QMediaPlayer::positionChanged,this,[=](qint64 pos)
    {
        ui->curlabel->setText(QString("%1:%2").arg(pos/1000/60,2,10,QChar('0')).arg(pos/1000%60));
        ui->horizontalSlider->setValue(pos);
    });

    //进度条
    connect(ui->horizontalSlider,&QSlider::sliderMoved,mediaPlayer,&QMediaPlayer::setPosition);
    //默认隐藏音量大小调节
    ui->verticalSlider->setVisible(false);
}
Widget::~Widget()
{
    delete ui;
}

//文件 阮
void Widget::on_pushButton_clicked()
{
    //选择音乐目录
    auto path=QFileDialog::getExistingDirectory(this,"选择路径","C:/Users/canlv/Desktop/photo");
    //获取路径内部的mp3文件与wav文件
    QDir dir(path);
    //将获取的文件通过listWidget保存
    auto musicList=dir.entryList(QStringList()<<"*.mp3"<<"*.wav");
    ui->listWidget_local->addItems(musicList);//展示音乐列表
    //循环输出音乐完整路径
    for(auto file:musicList)
    {
        playlist.append(QUrl::fromLocalFile(path+"/"+file));
    }
}

//开始和暂停 阮
void Widget::on_pushButton_2_clicked()
{
    switch(mediaPlayer->playbackState())
    {
    //没有歌曲播放时,获取鼠标点击行号;若无鼠标选中则默认为第一首歌
    case QMediaPlayer::PlaybackState::StoppedState:
    {
        if(ui->listWidget_local->currentRow()<0||ui->listWidget_local->currentRow()>=playlist.size())
        {
            curplayindex=0;
            mediaPlayer->setSource(playlist[curplayindex]);
            mediaPlayer->play();
            break;
        }
        else
        {
            curplayindex=ui->listWidget_local->currentRow();
            mediaPlayer->setSource(playlist[curplayindex]);
            mediaPlayer->play();
            break;
        }

    }
    //有歌曲播放，则暂停
    case QMediaPlayer::PlaybackState::PlayingState:
    {
        mediaPlayer->pause();
        break;
    }
    //暂停，则播放
    case QMediaPlayer::PlaybackState::PausedState:
    {
        mediaPlayer->play();
        break;
    }
    }
}

//播放模式 李
void Widget::on_pushButton_3_clicked()
{
    int random=QRandomGenerator::global()->bounded(playlist.size());
    curplayindex=random;
    ui->listWidget_local->setCurrentRow(curplayindex);
    mediaPlayer->setSource(playlist[curplayindex]);
    mediaPlayer->play();
}

//音量图标 徐
void Widget::on_pushButton_4_clicked()
{
    if(ui->verticalSlider->isVisible())
           {
                ui->verticalSlider->setVisible(false);
            }else
           {
                ui->verticalSlider->setVisible(true);
            }
}

//下一首 李
void Widget::on_pushButton_5_clicked()
{
    curplayindex++;
    if(curplayindex>=playlist.size())
    {
        curplayindex=0;
    }
    ui->listWidget_local->setCurrentRow(curplayindex);
    mediaPlayer->setSource(playlist[curplayindex]);
    mediaPlayer->play();

}

//上一首 李
void Widget::on_pushButton_6_clicked()
{
    curplayindex--;
    if(curplayindex<0)
    {
        curplayindex=playlist.size()-1;
    }
    ui->listWidget_local->setCurrentRow(curplayindex);
    mediaPlayer->setSource(playlist[curplayindex]);
    mediaPlayer->play();
}




//音量调节实现 徐
void Widget::on_verticalSlider_sliderMoved(int position)
{
    audioOutput->setVolume(position);
}

//删除本地列表指定歌曲 徐
void Widget::on_pushButton_7_clicked()
{

    QListWidgetItem *deleteItem = ui->listWidget_local->takeItem(curplayindex);
        ui->listWidget_local->removeItemWidget(deleteItem);
            delete deleteItem;
}

//移开鼠标音量调节消失 徐
void Widget::on_verticalSlider_sliderReleased()
{
    ui->verticalSlider->setVisible(false);
}

//local 徐
void Widget::on_pushButton_8_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

//收藏 徐
void Widget::on_pushButton_9_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

//鼠标双击添加收藏 徐
void Widget::on_listWidget_local_itemDoubleClicked(QListWidgetItem *item)
{
    ui->listWidget_local->row(item);
    mediaPlayer->play();
    ui->listWidget_history->addItem(item->text());
}

//本地点击播放 阮
void Widget::on_listWidget_local_currentRowChanged(int currentRow)
{
    curplayindex=currentRow;
    mediaPlayer->setSource(playlist[curplayindex]);
    mediaPlayer->play();
}

//收藏点击播放 阮
void Widget::on_listWidget_history_currentRowChanged(int currentRow)
{
    curplayindex=currentRow;
    mediaPlayer->setSource(playlist[curplayindex]);
    mediaPlayer->play();
}

