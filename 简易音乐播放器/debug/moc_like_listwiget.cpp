/****************************************************************************
** Meta object code from reading C++ file 'like_listwiget.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../like_listwiget.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'like_listwiget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
namespace {
struct qt_meta_stringdata_like_listwiget_t {
    uint offsetsAndSizes[16];
    char stringdata0[15];
    char stringdata1[32];
    char stringdata2[1];
    char stringdata3[6];
    char stringdata4[4];
    char stringdata5[25];
    char stringdata6[4];
    char stringdata7[15];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_like_listwiget_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_like_listwiget_t qt_meta_stringdata_like_listwiget = {
    {
        QT_MOC_LITERAL(0, 14),  // "like_listwiget"
        QT_MOC_LITERAL(15, 31),  // "send_likesong_list_Text_changed"
        QT_MOC_LITERAL(47, 0),  // ""
        QT_MOC_LITERAL(48, 5),  // "char*"
        QT_MOC_LITERAL(54, 3),  // "buf"
        QT_MOC_LITERAL(58, 24),  // "Rec_Widget_send_Likesong"
        QT_MOC_LITERAL(83, 3),  // "str"
        QT_MOC_LITERAL(87, 14)   // "send_list_Text"
    },
    "like_listwiget",
    "send_likesong_list_Text_changed",
    "",
    "char*",
    "buf",
    "Rec_Widget_send_Likesong",
    "str",
    "send_list_Text"
};
#undef QT_MOC_LITERAL
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_like_listwiget[] = {

 // content:
      10,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,   32,    2, 0x06,    1 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       5,    1,   35,    2, 0x0a,    3 /* Public */,
       7,    0,   38,    2, 0x0a,    5 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    6,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject like_listwiget::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_like_listwiget.offsetsAndSizes,
    qt_meta_data_like_listwiget,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_like_listwiget_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<like_listwiget, std::true_type>,
        // method 'send_likesong_list_Text_changed'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<char *, std::false_type>,
        // method 'Rec_Widget_send_Likesong'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        QtPrivate::TypeAndForceComplete<char *, std::false_type>,
        // method 'send_list_Text'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void like_listwiget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<like_listwiget *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->send_likesong_list_Text_changed((*reinterpret_cast< std::add_pointer_t<char*>>(_a[1]))); break;
        case 1: _t->Rec_Widget_send_Likesong((*reinterpret_cast< std::add_pointer_t<char*>>(_a[1]))); break;
        case 2: _t->send_list_Text(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (like_listwiget::*)(char * );
            if (_t _q_method = &like_listwiget::send_likesong_list_Text_changed; *reinterpret_cast<_t *>(_a[1]) == _q_method) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject *like_listwiget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *like_listwiget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_like_listwiget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int like_listwiget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void like_listwiget::send_likesong_list_Text_changed(char * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
