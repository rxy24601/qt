/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_6;
    QPushButton *pushButton_2;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QSlider *horizontalSlider;
    QSlider *verticalSlider;
    QLabel *curlabel;
    QLabel *totallabel;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QListWidget *listWidget_local;
    QWidget *page_2;
    QListWidget *listWidget_history;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName("Widget");
        Widget->resize(600, 800);
        Widget->setMinimumSize(QSize(600, 800));
        Widget->setMaximumSize(QSize(600, 800));
        Widget->setAutoFillBackground(false);
        Widget->setStyleSheet(QString::fromUtf8("QPushButton:hover{\n"
"	\n"
"	background-color: rgb(191, 191, 191);\n"
"}\n"
"#Widget\n"
"{\n"
"border-image: url(:/C:/Users/canlv/Desktop/photo/11.jpg);\n"
"}\n"
"	\n"
"\n"
""));
        widget = new QWidget(Widget);
        widget->setObjectName("widget");
        widget->setGeometry(QRect(10, 720, 541, 71));
        widget->setStyleSheet(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName("horizontalLayout");
        pushButton = new QPushButton(widget);
        pushButton->setObjectName("pushButton");
        pushButton->setMinimumSize(QSize(32, 32));
        pushButton->setMaximumSize(QSize(32, 32));
        pushButton->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/file.png);"));

        horizontalLayout->addWidget(pushButton);

        pushButton_3 = new QPushButton(widget);
        pushButton_3->setObjectName("pushButton_3");
        pushButton_3->setMinimumSize(QSize(32, 32));
        pushButton_3->setMaximumSize(QSize(32, 32));
        pushButton_3->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/random loop.png);"));

        horizontalLayout->addWidget(pushButton_3);

        pushButton_6 = new QPushButton(widget);
        pushButton_6->setObjectName("pushButton_6");
        pushButton_6->setMinimumSize(QSize(32, 32));
        pushButton_6->setMaximumSize(QSize(32, 32));
        pushButton_6->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/previous.png);"));

        horizontalLayout->addWidget(pushButton_6);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName("pushButton_2");
        pushButton_2->setMinimumSize(QSize(32, 32));
        pushButton_2->setMaximumSize(QSize(32, 32));
        pushButton_2->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/play.png);"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton_5 = new QPushButton(widget);
        pushButton_5->setObjectName("pushButton_5");
        pushButton_5->setMinimumSize(QSize(32, 32));
        pushButton_5->setMaximumSize(QSize(32, 32));
        pushButton_5->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/next.png);"));

        horizontalLayout->addWidget(pushButton_5);

        pushButton_4 = new QPushButton(widget);
        pushButton_4->setObjectName("pushButton_4");
        pushButton_4->setMinimumSize(QSize(32, 32));
        pushButton_4->setMaximumSize(QSize(32, 32));
        pushButton_4->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/voice.png);"));

        horizontalLayout->addWidget(pushButton_4);

        horizontalSlider = new QSlider(Widget);
        horizontalSlider->setObjectName("horizontalSlider");
        horizontalSlider->setGeometry(QRect(10, 680, 541, 22));
        horizontalSlider->setOrientation(Qt::Horizontal);
        verticalSlider = new QSlider(Widget);
        verticalSlider->setObjectName("verticalSlider");
        verticalSlider->setGeometry(QRect(560, 680, 22, 101));
        verticalSlider->setOrientation(Qt::Vertical);
        curlabel = new QLabel(Widget);
        curlabel->setObjectName("curlabel");
        curlabel->setGeometry(QRect(10, 640, 101, 21));
        totallabel = new QLabel(Widget);
        totallabel->setObjectName("totallabel");
        totallabel->setGeometry(QRect(500, 640, 77, 22));
        label_4 = new QLabel(Widget);
        label_4->setObjectName("label_4");
        label_4->setGeometry(QRect(230, 20, 34, 22));
        label_3 = new QLabel(Widget);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(170, 20, 34, 22));
        label_2 = new QLabel(Widget);
        label_2->setObjectName("label_2");
        label_2->setGeometry(QRect(100, 20, 34, 22));
        label = new QLabel(Widget);
        label->setObjectName("label");
        label->setGeometry(QRect(41, 21, 68, 22));
        stackedWidget = new QStackedWidget(Widget);
        stackedWidget->setObjectName("stackedWidget");
        stackedWidget->setGeometry(QRect(39, 59, 491, 581));
        page = new QWidget();
        page->setObjectName("page");
        listWidget_local = new QListWidget(page);
        listWidget_local->setObjectName("listWidget_local");
        listWidget_local->setGeometry(QRect(0, 0, 481, 571));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName("page_2");
        listWidget_history = new QListWidget(page_2);
        listWidget_history->setObjectName("listWidget_history");
        listWidget_history->setGeometry(QRect(0, 0, 491, 581));
        stackedWidget->addWidget(page_2);
        layoutWidget = new QWidget(Widget);
        layoutWidget->setObjectName("layoutWidget");
        layoutWidget->setGeometry(QRect(530, 90, 66, 481));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_7 = new QPushButton(layoutWidget);
        pushButton_7->setObjectName("pushButton_7");
        pushButton_7->setMinimumSize(QSize(64, 64));
        pushButton_7->setMaximumSize(QSize(64, 64));
        pushButton_7->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/delete.png);"));

        verticalLayout->addWidget(pushButton_7);

        pushButton_8 = new QPushButton(layoutWidget);
        pushButton_8->setObjectName("pushButton_8");
        pushButton_8->setMinimumSize(QSize(64, 64));
        pushButton_8->setMaximumSize(QSize(64, 64));
        pushButton_8->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/playlist.png);"));

        verticalLayout->addWidget(pushButton_8);

        pushButton_9 = new QPushButton(layoutWidget);
        pushButton_9->setObjectName("pushButton_9");
        pushButton_9->setMinimumSize(QSize(64, 64));
        pushButton_9->setMaximumSize(QSize(64, 64));
        pushButton_9->setStyleSheet(QString::fromUtf8("border-image: url(:/C:/Users/canlv/Desktop/photo/liked.jpg);"));

        verticalLayout->addWidget(pushButton_9);

        layoutWidget->raise();
        label_4->raise();
        label_3->raise();
        label_2->raise();
        label->raise();
        horizontalSlider->raise();
        totallabel->raise();
        curlabel->raise();
        widget->raise();
        verticalSlider->raise();
        stackedWidget->raise();

        retranslateUi(Widget);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        pushButton->setText(QString());
        pushButton_3->setText(QString());
        pushButton_6->setText(QString());
        pushButton_2->setText(QString());
        pushButton_5->setText(QString());
        pushButton_4->setText(QString());
        curlabel->setText(QCoreApplication::translate("Widget", "00.00", nullptr));
        totallabel->setText(QCoreApplication::translate("Widget", "00.00", nullptr));
        label_4->setText(QCoreApplication::translate("Widget", "\344\270\223\350\276\221", nullptr));
        label_3->setText(QCoreApplication::translate("Widget", "\346\255\214\346\211\213", nullptr));
        label_2->setText(QCoreApplication::translate("Widget", "\346\255\214\345\220\215", nullptr));
        label->setText(QCoreApplication::translate("Widget", "\347\274\226\345\217\267", nullptr));
        pushButton_7->setText(QString());
        pushButton_8->setText(QString());
        pushButton_9->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
